$(function() {
	// 鼠标滑动效果
	$('.fav-title-list').mouseover(function() {
		$(this).css('background', '#f2f2f2');
		$(this).children().css('color', '#000000');
		$(this).children('.title-list-action').css('visibility', 'visible');
	});
	$('.fav-title-list').mouseleave(function() {
		$(this).css('background', '');
		$(this).children().css('color', '');
		$(this).children('.title-list-action').css('visibility', 'hidden');
	});

	// 取消收藏弹出框
	var deletefavtitleid;
	$('.delete').click(function() {
		// $('.fav-title-list').undelegate();
		// console.log($(this).attr('title'));
		$('#delete-modal-body strong').text($(this).attr('title'));
		deletefavtitleid = $(this).attr('nodefavoriteid');
	})

	$('#deletefavtitle').click(function() {
		$.ajax({
			url : app.contextPath + '/deletefavtitle.json',
			dataType : 'json',
			type : 'PostContent',
			data : {
				nodefavoriteid : deletefavtitleid
			}
		}).done(function(data) {
			location.reload();
		})
	})

	var nodefavoriteid;
	$('.moveto')
			.click(
					function() {
						$('#favlist-dropdown-menu').empty();
						$('#favlistdropdowninput').attr('select', '');
						$('#favlistdropdowninput').text(
								$('#favlistdropdowninput').attr('favoritename'));
						nodefavoriteid = $(this).attr('nodefavoriteid');
						$
								.ajax({
									url : 'favoritename.json',
									dataType : 'json'
								})
								.done(
										function(data) {
//											 console.log(data);
											for (i in data.FAV_LIST) {
												$('#favlist-dropdown-menu')
														.append(
																'<li favoriteid = \"'
																		+ data.FAV_LIST[i].ID
																		+ '\" onclick=\"$(\'#favlistdropdowninput\').text($(this).text());$(\'#favlistdropdowninput\').attr(\'select\',$(this).attr(\'favoriteid\'))\"><a>'
																		+ data.FAV_LIST[i].FAVORITE_NAME
																		+ '</a></li>');
											}
										})
					})

	$('#movetoFav').click(function() {
		if ($('#favlistdropdowninput').attr('select') != '') {
//			console.log("11111111111111111");
//			console.log($('#favlistdropdowninput').attr('select'));
			$.ajax({
				url : app.contextPath + '/updatefavid.json',
				dataType : 'json',
				type : 'PostContent',
				data : {
					nodefavoriteid : nodefavoriteid,
					favoriteid : $('#favlistdropdowninput').attr('select')
				}
			}).done(function(data) {
				location.reload();
			})
		}
		$('#favlistdropdowninput').attr('select', '');
	})
	
		// 个人中心删除文章
	var deletenodeid;
	var userid;
	$('.delete').click(function() {
		// $('.fav-title-list').undelegate();
		// console.log($(this).attr('title'));
		$('#delete-modal-body strong').text($(this).attr('title'));
		deletenodeid = $(this).attr('nodefavoriteid');
		userid = $(this).attr('userid');
//		 console.log(deletenodeid);
//		 console.log(userid);
	})

	$('#deleteNode').click(function() {
	//	console.log('1111');
		$.ajax({
			url : app.contextPath + '/post/delete.json',
			dataType : 'json',
			type : 'PostContent',
			data : {
				nodeid:deletenodeid,
					userid:userid
			}
		}).done(function(data) {
	//		console.log('22222');
			location.reload();
		})
	})
	
	$('#deleteReply').click(function() {
			console.log(deletenodeid);
		$.ajax({
			url : app.contextPath + '/delreply.json',
			dataType : 'json',
			type : 'PostContent',
			data : {
				replyid:deletenodeid,
				s:'1'
			}
		}).done(function(data) {
				console.log('22222');
			location.reload();
		})
	})
	
})