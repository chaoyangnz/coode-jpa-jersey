$(function() {

    //-----消息数提示--------------
    $.ajax({
        url: app.contextPath + '/notify/number.json',
        dataType: 'json'
    }).done(function( data ) {
//        console.log(data)
        if (data.unread && data.unread != 0) {
            $('#coode-message').removeClass('hide')
            $('#coode-message').text(data.unread)
        }
    });

    //------消息弹出框-----------------
    $('[data-toggle="popover"]').popover();

    // 点击popover外部自动关闭popover
    $('html').on('click', function (e) {
        $('[data-toggle="popover"]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
//            console.log($(this).is(e.target))
//            console.log($(this).has(e.target).length)
//            console.log($('.popover').has(e.target).length)
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

    $('#coode-message-button').on('shown.bs.popover', function () {
        $.ajax({
            url: app.contextPath + '/notify',
            dataType: 'html',
            success: function(data) {
                $('#coode-message-popover .popover-content').html(data)
            }
        })
    })

    //--------成功提示框提示框-----------
    window.setTimeout(function() {
        $(".alert-success").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 5000);

    // 搜索
    $('.coode-search-box').keydown(function(e) {
        //keyCode==13 为回车
        if (e.keyCode == 13) {
            $(this).last('form').submit()
        }
    });
    
     
    setInterval("sos()",300000)

    $('#search-icon').click(function() {
        $(this).closest('form').submit()
    })
})

    function sos(){
        $.ajax({
            url: app.contextPath + '/seekSOS',
            dataType: 'html',
            success: function(data) {
        	//  alert(data);
              $("body").append(data);
              $("div.modal.fade.ext").modal('show');
            }
        })
    }


$(document).delegate("#help", "click", function(){
	var $this = $(this);
	var tag_name = $("#content").val();
	var html = null;
	$.ajax({
		url:'sos',
		type : 'PostContent',
		dataType: 'html',
		async : true,
		data : {
		'tagName' : tag_name
	}
	}).done(function(data) {
		html = data;
		$this.popover({
			trigger:'click',
			placement : 'right',
			html: 'true',
			content : html,
			animation: 'true'
		});
	})
});

//$("#SOSModal #helper").on("blur",function(){
//	$(this).popover('hide');
//})

$('#SOSModal').on('hidden.bs.modal', function (e) {
	//  alert('hidden.bs.modal')
	  $("#SOSModal form").get(0).reset();
	  $("#help").popover('destroy');
	})

 $("#SOSModal button[type='submit']").click(function (){
		 $('form.well').submit();
		 $("#SOSModal form").get(0).reset();
		 $("#help").popover('destroy');
		 $("#help").popover('hide');
		 $("#SOSModal").modal('hide')
	});




