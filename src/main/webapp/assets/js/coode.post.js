$(document).ready(function(){
	var commentid = window.location.hash;
	if(commentid){
		console.log(commentid);
		$(commentid).parents('.collapse').removeClass("collapse");
		var pos = $(commentid).offset().top;
		$("html,body").animate({scrollTop: pos-690}, 1000);
		setTimeout(function(){
			for(var i=0; i<3; i++)
				$(commentid).fadeOut(200).fadeIn(200);
		},1000);
	}
	
	$("#commentsort li a").click(function() {
        var $this = $(this);
        APP['sort'] = $this.attr('sort');
        var $a = $this.parent().parent().prev()
        $a.text($this.text());
        $a.append("<span class='caret'>");
        
        $('#comments').empty();
        getReply();
    });
	
	$(document).delegate('.delcomment', 'click', function(){
		var commentid = $(this).attr('comment');
		console.log(commentid);
		var url = $(this).attr('url');
		var content = $("#content"+commentid).text();
		$("#commentcontent").text(content);
		$("#delcommentcontent").attr("href", url)
	});
	
	$(document).delegate("#deletepost", "click", function(){
		deletePost(APP['postid']);
	});
	
	$(document).delegate(".watchuserinfo", "mouseover", function(){
		var $this = $(this);
		var author_id = $this.attr('author');
		var userinfo = null;
		$.ajax({
			url: app.contextPath + '/usercard',
			type : 'PostContent',
			dataType: 'html',
			async : false,
			data : {
				userid : author_id
			}
		}).done(function(data) {
			userinfo = data;
		});
		
		$this.popover({
            trigger:'mouseenter',
            placement : 'right', //placement of the popover. also can use top, bottom, left or right
            html: 'true', //needed to show html of course
            content : userinfo,
            animation: true
        }).on("mouseenter", function () {
            $this.popover("show");
            $(".popover").on("mouseleave", function () {
            	$this.popover('hide');
            });
        }).on("mouseleave", function () {
            setTimeout(function () {
                if (!$(".popover:hover").length) {
                	$this.popover("hide")
                }
            }, 100);
        });
	});
	
	$('#ta').bind("foucs keyup input paste",function(){
		var word = $(this).val();
		$('#num').text(140 - word.length);
	});
});

function getReply() {
	$.ajax({
		url : 'qryreply',
		dataType : 'html',
		type : 'PostContent',
		data : {
			nodeid : APP['postid'],
			sort : APP['sort'],
			type : APP['posttype']
		}
	}).done(function(data) {
		$('#comments').html(data)
	});	
}

function deletePost(nodeid) {
	$.ajax({
		url: app.contextPath + '/post/delete.json',
		dataType:'json',
		type : 'PostContent',
		data : {
			nodeid : nodeid
		}
	}).done(function(data){
		if(1==APP['posttype']){
			location.href = app.contextPath + "/?s=article";
		} else if(5==APP['posttype']){
			location.href = app.contextPath + "/?s=idea";
		} else {
			location.href = app.contextPath + "/?s=qsh";
		}
	})
}
var span;
$("#favbtn").hover(function(){
	 span = $("#favbtn").html();
	if($("#favbtn span").hasClass("glyphicon-star-empty"))
	{
		$("#favbtn").addClass("btn-primary").html("<small>收藏<small>");
	}
},function(){
	$("#favbtn").html(span).removeClass("btn-primary");
	
})
var span2;
$("#favbtn").hover(function(){
	 span2 = $("#favbtn").html();
	if($("#favbtn span").hasClass("glyphicon-star"))
	{
		$("#favbtn").addClass("btn-primary").html("<small>取消收藏<small>");
	}
},function(){
	$("#favbtn").html(span).removeClass("btn-primary");
	
})