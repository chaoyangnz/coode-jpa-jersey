package com.bocom.coode.dao.entity;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "USER")
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "USER_NAME")
    private String userName;
    @Column(name = "NICK_NAME")
    private String nickName;
    @Column(name = "POSITION")
    private String position;
    @Column(name = "PROJ_EXP")
    private String projectExperience;
    @Column(name = "HEAD_ID")
    private String headId;
    @Column(name = "DEPT")
    private String department;
    @Column(name = "DEPT_VISIBLE")
    private Short departmentVisible = 1;
    @Column(name = "STAT_POST")
    private Integer statPost = 0;
    @Column(name = "STAT_REPLY")
    private Integer statReply = 0;
    @Column(name = "CREDIT")
    private Integer credit = 0;
    @Column(name = "NOTIFY_AT")
    private Short notifyAt = 0;
    @Column(name = "NOTIFY_REPLY")
    private Short notifyReply = 0;
    @Column(name = "NOTIFY_FAVORITE")
    private Short notifyFavorite = 0;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getProjectExperience() {
        return projectExperience;
    }

    public void setProjectExperience(String projectExperience) {
        this.projectExperience = projectExperience;
    }

    public String getHeadId() {
        return headId;
    }

    public void setHeadId(String headId) {
        this.headId = headId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Short getDepartmentVisible() {
        return departmentVisible;
    }

    public void setDepartmentVisible(Short departmentVisible) {
        this.departmentVisible = departmentVisible;
    }

    public Integer getStatPost() {
        return statPost;
    }

    public void setStatPost(Integer statPost) {
        this.statPost = statPost;
    }

    public Integer getStatReply() {
        return statReply;
    }

    public void setStatReply(Integer statReply) {
        this.statReply = statReply;
    }

    public Integer getCredit() {
        return credit;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    public Short getNotifyAt() {
        return notifyAt;
    }

    public void setNotifyAt(Short notifyAt) {
        this.notifyAt = notifyAt;
    }

    public Short getNotifyReply() {
        return notifyReply;
    }

    public void setNotifyReply(Short notifyReply) {
        this.notifyReply = notifyReply;
    }

    public Short getNotifyFavorite() {
        return notifyFavorite;
    }

    public void setNotifyFavorite(Short notifyFavorite) {
        this.notifyFavorite = notifyFavorite;
    }
}
