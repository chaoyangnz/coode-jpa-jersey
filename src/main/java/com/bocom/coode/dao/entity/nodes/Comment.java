package com.bocom.coode.dao.entity.nodes;

import com.bocom.coode.dao.entity.Node;
import org.springframework.stereotype.Component;

import javax.persistence.*;

@Entity

@DiscriminatorValue("7")
public class Comment extends FragmentContent {

    @Column(name = "COMMENTABLE_ID")
    private Long commentableId;

    public Long getCommentableId() {
        return commentableId;
    }

    public void setCommentableId(Long commentableId) {
        this.commentableId = commentableId;
    }
}
