package com.bocom.coode.dao.entity.nodes;

import com.bocom.coode.dao.entity.Node;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class FragmentContent extends Node {
    @Column(name = "POST_ID")
    private Long postId;

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }
}
