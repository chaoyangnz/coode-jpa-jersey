package com.bocom.coode.dao.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "NODE")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "NODE_TYPE")
public abstract class Node implements Serializable {
    public final static Integer NT_ARTICLE    = 1;
    public final static Integer NT_QUESTION   = 2;
    public final static Integer NT_SHARE      = 3;
    public final static Integer NT_HANGOUT    = 4;
    public final static Integer NT_IDEA       = 5;
    public final static Integer NT_ANSWER     = 6;
    public final static Integer NT_COMMENT    = 7;

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public final static int ST_PUBLIC       = 1;
    public final static int ST_DRAFT        = 2;

//    public final static int IF_DRAFT        = 1;

    public boolean isPost() {
        return nodeType == NT_QUESTION || nodeType == NT_SHARE || nodeType == NT_HANGOUT || nodeType == NT_IDEA;
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    @Column(name = "NODE_TYPE")
    private Integer nodeType;
    @Column(name = "TITLE")
    private String title;
    @Column(name = "CONTENT")
    private String content;
    @Column(name = "STAT_VIEWS")
    private Integer statViews = 0;
    @Column(name = "STAT_COMMENTS")
    private Integer statComments = 0;
    @Column(name = "STAT_FAVORITES")
    private Integer statFavorites = 0;
    @Column(name = "STAT_UPVOTES")
    private Integer statUpvotes = 0;
    @Column(name = "STAT_ANSWERS")
    private Integer statAnswers = 0;
    @Column(name = "CREATED_AT")
    private String createdAt;
    @Column(name = "UPDATED_AT")
    private String updatedAt;
    @Column(name = "CREATED_BY")
    private Long createdBy;
    @Column(name = "UPDATED_BY")
    private Long updatedBy;
    @Column(name = "STATUS")
    private Integer status;
    @Column(name = "INDEX_FLAG")
    private Integer indexFlag;
    @Column(name = "INDEX_LOCK")
    private Integer indexLock;
    @Column(name = "CUSTOM_ATTRIBUTES")
    private String customAttributes;

    @ManyToMany(cascade={CascadeType.PERSIST})
    @JoinTable(name = "NODE_TAG",
            joinColumns = @JoinColumn(name = "NODE_ID"),
            inverseJoinColumns = @JoinColumn(name = "TAG_ID"))
    private List<Tag> tags;

    public String getCustomAttributes() {
        return customAttributes;
    }

    public void setCustomAttributes(String customAttributes) {
        this.customAttributes = customAttributes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNodeType() {
        return nodeType;
    }

    public void setNodeType(Integer nodeType) {
        this.nodeType = nodeType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getStatViews() {
        return statViews;
    }

    public void setStatViews(Integer statViews) {
        this.statViews = statViews;
    }

    public Integer getStatComments() {
        return statComments;
    }

    public void setStatComments(Integer statComments) {
        this.statComments = statComments;
    }

    public Integer getStatFavorites() {
        return statFavorites;
    }

    public void setStatFavorites(Integer statFavorites) {
        this.statFavorites = statFavorites;
    }

    public Integer getStatUpvotes() {
        return statUpvotes;
    }

    public void setStatUpvotes(Integer statUpvotes) {
        this.statUpvotes = statUpvotes;
    }

    public Integer getStatAnswers() {
        return statAnswers;
    }

    public void setStatAnswers(Integer statAnswers) {
        this.statAnswers = statAnswers;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIndexFlag() {
        return indexFlag;
    }

    public void setIndexFlag(Integer indexFlag) {
        this.indexFlag = indexFlag;
    }

    public Integer getIndexLock() {
        return indexLock;
    }

    public void setIndexLock(Integer indexLock) {
        this.indexLock = indexLock;
    }
}
