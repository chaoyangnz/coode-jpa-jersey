package com.bocom.coode.dao.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "TAG")
public class Tag {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "IS_ROOT")
    private Integer isRoot = 0;
    @Column(name = "STAT_POST")
    private Integer statPost = 0;
    @Column(name = "STAT_USER")
    private Integer statUser = 0;

    @ManyToMany(mappedBy = "tags")
    private List<Node> nodes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIsRoot() {
        return isRoot;
    }

    public void setIsRoot(Integer isRoot) {
        this.isRoot = isRoot;
    }

    public Integer getStatPost() {
        return statPost;
    }

    public void setStatPost(Integer statPost) {
        this.statPost = statPost;
    }

    public Integer getStatUser() {
        return statUser;
    }

    public void setStatUser(Integer statUser) {
        this.statUser = statUser;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }
}
