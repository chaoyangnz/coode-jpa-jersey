package com.bocom.coode.dao.entity.nodes;

import com.bocom.coode.dao.entity.Node;

import javax.persistence.*;

@MappedSuperclass
public abstract class PostContent extends Node {
    @Column(name = "TITLE")
    private String title;

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    public static PostContent newInstance(int nodeType) {
        PostContent node = null;
        if(nodeType == NT_QUESTION) {
            node = new Question();
        } else if(nodeType == NT_HANGOUT) {
            node = new Hangout();
        } else if(nodeType == NT_SHARE) {
            node = new Share();
        } else {
            assert false : "This node type is unacceptable";
        }
        node.setNodeType(nodeType);
        return node;
    }
}
