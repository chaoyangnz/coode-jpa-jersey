package com.bocom.coode.helpers;

import org.glassfish.jersey.server.mvc.Viewable;

import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class Renders {

    public static Response redirect(String url) {
        try {
            return Response.status(302).location(new URI(url)).build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Viewable render(String template, Map<String, ?> bindings) {
        return new Viewable(template, bindings);
    }

    public static Viewable render(String template, Object...bindingArgs) {
        Map<String, Object> bindings = new HashMap<String, Object>();

        for(int i = 0; i < bindingArgs.length; ++i) {
            String key = bindingArgs[i].toString();
            if(i+1 < bindingArgs.length) {
                bindings.put(key, bindingArgs[i+1]);
            } else {
                break;
            }
        }

        return render(template, bindings);
    }
}
