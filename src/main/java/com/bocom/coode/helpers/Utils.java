package com.bocom.coode.helpers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class Utils {

    public static String currentTime() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
        return df.format(new Date());
    }

    public static Long currentUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        return (Long)session.getAttribute("userId");
    }
}
