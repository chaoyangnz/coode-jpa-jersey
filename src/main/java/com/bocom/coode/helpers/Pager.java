package com.bocom.coode.helpers;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: 杨超
 * Date: 14-4-21 下午4:54
 */
public class Pager {

    //记录相关
    private int begin; //从0开始
    private int end;
    private int count;

    //分页相关
    private int pageSize;
    private int pageCount;
    private int currentPage;//从1开始
    private int prevPage;
    private int nextPage;
    private List<Integer> pageRange = new ArrayList<Integer>();
    private boolean isFirst;
    private boolean isLast;
    private boolean isEmpty = true;

    public  Pager(int pageSize, int currentPage) {
        this.pageSize = pageSize;
        this.currentPage = currentPage;
    }

    public int getBegin() {
        return begin;
    }

    public int getEnd() {
        return end;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;

        this.begin = pageSize * (currentPage - 1)+1 ;
        this.end = Math.min(begin + pageSize - 1, count);

        this.pageCount = count/pageSize;
        if(count%pageSize != 0) {
            this.pageCount += 1;
        }
        this.prevPage = currentPage == 1  ? currentPage : currentPage - 1;
        this.nextPage = currentPage == this.pageCount  ? currentPage : currentPage + 1;
        this.isFirst = currentPage == 1;
        this.isLast = currentPage == pageCount;
        this.isEmpty = (count == 0);

        List<Integer> list = new ArrayList<Integer>();
        for(int i = 1; i <= pageCount; ++i) {
            if(i == 1 || i == pageCount || i == this.currentPage) {
                list.add(i);
            } else if(i < this.currentPage && i + 4 >= this.currentPage) {
                list.add(i);
            } else if(i > this.currentPage && i - 4 <= this.currentPage) {
                list.add(i);
            } else {
                list.add(-1);
            }
        }

        for(int i = 0; i < list.size(); ++i) {
            if(list.get(i) == -1 && list.get(i-1) == -1) {
            } else {
                this.pageRange.add(list.get(i));
            }
        }
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getPageCount() {
        return pageCount;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getPrevPage() {
        return prevPage;
    }

    public int getNextPage() {
        return nextPage;
    }

    public List getPageRange() {
        return pageRange;
    }

    public boolean isFirst() {
        return isFirst;
    }

    public boolean isLast() {
        return isLast;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

}
