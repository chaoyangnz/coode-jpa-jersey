package com.bocom.coode.resource;

import com.bocom.coode.helpers.Pager;
import org.glassfish.jersey.server.mvc.Viewable;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.util.HashMap;
import java.util.Map;

/**
 * Home, Searching, Notification, SOS
 */
@Path("/")
@Produces(MediaType.TEXT_HTML)
public class SiteResource {
    @GET
    @Path("/")
    public Viewable home() {
        Map<String, Object> bindings = new HashMap<String, Object>();
        Pager pager = new Pager(20, 1);
        pager.setCount(0);
        bindings.put("pager", pager);
        return new Viewable("/home.html", bindings);
    }
}

