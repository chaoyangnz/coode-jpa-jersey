package com.bocom.coode.resource;

import com.bocom.coode.dao.entity.Node;
import com.bocom.coode.helpers.Renders;
import com.bocom.coode.helpers.Utils;
import com.bocom.coode.service.NodeService;
import org.glassfish.jersey.server.mvc.Viewable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.swing.text.View;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

@Component
@Path("/nodes")
@Produces(MediaType.TEXT_HTML)
public class NodesResource {

    @Autowired
    private NodeService nodeService;

    @GET
    @Path("/")
    public List<Node> listNodes() {
        return Collections.emptyList();
    }

    @GET
    @Path("tags/{tagId}")
    public List<Node> listNodesByTags(@PathParam("tagId") Long tagId) {
        return null;
    }

    @GET
    @Path("{nodeId}")
    @Produces(MediaType.TEXT_HTML)
    public Viewable showNode(@PathParam("nodeId") long nodeId) {

        return Renders.render("/post/show.html", "node", nodeService.queryPost(nodeId));
    }

    @GET
    @Path("new")
    @Produces(MediaType.TEXT_HTML)
    public Viewable newPost() {
        return Renders.render("/post/new.html");
    }

    @POST
    public Response createPost(@FormParam("type") int nodeType,
                               @FormParam("title") String title,
                               @FormParam("content") String content,
                               @FormParam("tags") String tags,
                               @Context HttpServletRequest request) {

        Node node = nodeService.createPost(Utils.currentUser(request), nodeType, title, content, tags);

        return Renders.redirect("nodes/" + node.getId());
    }

    @POST
    @Path("{postId}/{commentableId}/comment")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createComment() {
        return null;
    }

    @POST
    @Path("{questionId}/answer")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createAnswer() {
        return null;
    }

}
