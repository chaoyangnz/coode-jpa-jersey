package com.bocom.coode.resource;

import com.bocom.coode.dao.entity.User;
import com.bocom.coode.helpers.Renders;
import com.bocom.coode.service.UserService;
import org.glassfish.jersey.server.mvc.Viewable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Path("/user")
@Produces(MediaType.TEXT_HTML)
public class UserResource {

    @Autowired
    private UserService userService;


    @GET
    @Path("login")
    public Viewable login() {
        Map<String, Object> bindings = new HashMap<String, Object>();
        return new Viewable("/login.html", bindings);
    }

    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response loginSubmit(@Context HttpServletRequest request,
                                @FormParam("username") String userName,
                                @FormParam("password") String passWord) {

        userService.authenticate(userName, passWord);
        User user = userService.queryOrCreateUser(userName);

        HttpSession session = request.getSession();
        session.setAttribute("logined", true);
        session.setAttribute("userId", user.getId());
        session.setAttribute("userName", user.getUserName());

        return Renders.redirect("/");
    }

    @GET
    @Path("logout")
    public Response logout(@Context HttpServletRequest request) {
        HttpSession session = request.getSession();
        Object o = session.getAttribute("logined");
        if(o != null && o.equals(true)) {
            session.setAttribute("logined", false);
            session.invalidate();
        }

        return Renders.redirect("/user/login");
    }
}
