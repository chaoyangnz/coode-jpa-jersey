package com.bocom.coode;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class StaticServlet extends HttpServlet {
	protected static final int deflateThreshold = 4096;
	protected static final int bufferSize = 4096;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		lookup(req).respondGet(resp);
	}

	protected void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doGet(req, resp);
	}

	protected void doHead(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		try {
			lookup(req).respondHead(resp);
		} catch (UnsupportedOperationException e) {
			super.doHead(req, resp);
		}
	}

	protected long getLastModified(HttpServletRequest req) {
		return lookup(req).getLastModified();
	}

	protected LookupResult lookup(HttpServletRequest req) {
		LookupResult r = (LookupResult) req.getAttribute("lookupResult");
		if (r == null) {
			r = lookupNoCache(req);
			req.setAttribute("lookupResult", r);
		}
		return r;
	}

	protected LookupResult lookupNoCache(HttpServletRequest req) {
		String path = getPath(req);
		if (isForbidden(path))
			return new Error(403, "Forbidden");
		URL url;
		try {
			url = getServletContext().getResource(path);
		} catch (MalformedURLException e) {
			return new Error(400, "Malformed com.bocom.p2p.suppot.routes.path");
		}
		if (url == null) {
			return new Error(404, "Not found");
		}
		String mimeType = getMimeType(path);

		String realpath = getServletContext().getRealPath(path);
		if (realpath != null) {
			File f = new File(realpath);
			if (!f.isFile()) {
				return new Error(403, "Forbidden");
			}
			return new StaticFile(f.lastModified(), mimeType, (int) f.length(),
					acceptsDeflate(req), url);
		}
		try {
			ZipEntry ze = ((JarURLConnection) url.openConnection())
					.getJarEntry();
			if (ze != null) {
				if (ze.isDirectory()) {
					return new Error(403, "Forbidden");
				}
				return new StaticFile(ze.getTime(), mimeType,
						(int) ze.getSize(), acceptsDeflate(req), url);
			}

			return new StaticFile(-1L, mimeType, -1, acceptsDeflate(req), url);
		} catch (ClassCastException e) {
			return new StaticFile(-1L, mimeType, -1, acceptsDeflate(req), url);
		} catch (IOException e) {
		}
		return new Error(500, "Internal server error");
	}

	protected String getPath(HttpServletRequest req) {
		String servletPath = req.getServletPath();
		String pathInfo = (String) coalesce(new String[] { req.getPathInfo(),
				"" });
		return servletPath + pathInfo;
	}

	protected boolean isForbidden(String path) {
		String lpath = path.toLowerCase();
		return (lpath.startsWith("/web-inf/"))
				|| (lpath.startsWith("/meta-inf/"));
	}

	protected String getMimeType(String path) {
		return (String) coalesce(new String[] {
				getServletContext().getMimeType(path),
				"application/octet-stream" });
	}

	protected static boolean acceptsDeflate(HttpServletRequest req) {
		String ae = req.getHeader("Accept-Encoding");
		return (ae != null) && (ae.contains("gzip"));
	}

	protected static boolean deflatable(String mimetype) {
		return (mimetype.startsWith("text/"))
				|| (mimetype.equals("application/postscript"))
				|| (mimetype.startsWith("application/ms"))
				|| (mimetype.startsWith("application/vnd"))
				|| (mimetype.endsWith("xml"));
	}

	protected static void transferStreams(InputStream is, OutputStream os)
			throws IOException {
		try {
			byte[] buf = new byte[4096];
			int bytesRead;
			while ((bytesRead = is.read(buf)) != -1)
				os.write(buf, 0, bytesRead);
		} finally {
			is.close();
			os.close();
		}
	}

	private static <T> T coalesce(T[] ts) {
		for (Object t : ts)
			if (t != null)
				return (T) t;
		return null;
	}

	public static class StaticFile implements LookupResult {
		protected final long lastModified;
		protected final String mimeType;
		protected final int contentLength;
		protected final boolean acceptsDeflate;
		protected final URL url;

		public StaticFile(long lastModified, String mimeType,
				int contentLength, boolean acceptsDeflate, URL url) {
			this.lastModified = lastModified;
			this.mimeType = mimeType;
			this.contentLength = contentLength;
			this.acceptsDeflate = acceptsDeflate;
			this.url = url;
		}

		public long getLastModified() {
			return this.lastModified;
		}

		protected boolean willDeflate() {
			return (this.acceptsDeflate)
					&& (StaticServlet.deflatable(this.mimeType))
					&& (this.contentLength >= 4096);
		}

		protected void setHeaders(HttpServletResponse resp) {
			resp.setStatus(200);
			resp.setContentType(this.mimeType);
			if ((this.contentLength >= 0) && (!willDeflate()))
				resp.setContentLength(this.contentLength);
		}

		public void respondGet(HttpServletResponse resp) throws IOException {
			setHeaders(resp);
			OutputStream os;
			if (willDeflate()) {
				resp.setHeader("Content-Encoding", "gzip");
				os = new GZIPOutputStream(resp.getOutputStream(), 4096);
			} else {
				os = resp.getOutputStream();
			}
			StaticServlet.transferStreams(this.url.openStream(), os);
		}

		public void respondHead(HttpServletResponse resp) {
			if (willDeflate())
				throw new UnsupportedOperationException();
			setHeaders(resp);
		}
	}

	public static class Error implements LookupResult {
		protected final int statusCode;
		protected final String message;

		public Error(int statusCode, String message) {
			this.statusCode = statusCode;
			this.message = message;
		}

		public long getLastModified() {
			return -1L;
		}

		public void respondGet(HttpServletResponse resp) throws IOException {
			resp.sendError(this.statusCode, this.message);
		}

		public void respondHead(HttpServletResponse resp) {
			throw new UnsupportedOperationException();
		}
	}

	public static abstract interface LookupResult {
		public abstract void respondGet(
                HttpServletResponse paramHttpServletResponse)
				throws IOException;

		public abstract void respondHead(
                HttpServletResponse paramHttpServletResponse);

		public abstract long getLastModified();
	}
}