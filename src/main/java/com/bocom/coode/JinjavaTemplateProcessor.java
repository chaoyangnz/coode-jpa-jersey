package com.bocom.coode;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.hubspot.jinjava.Jinjava;
import com.hubspot.jinjava.JinjavaConfig;
import com.hubspot.jinjava.interpret.JinjavaInterpreter;
import com.hubspot.jinjava.loader.ResourceLocator;
import org.glassfish.jersey.server.mvc.Viewable;
import org.glassfish.jersey.server.mvc.spi.AbstractTemplateProcessor;
import org.joda.time.DateTimeZone;
import org.jvnet.hk2.annotations.Optional;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.Map;


@Provider
public class JinjavaTemplateProcessor extends AbstractTemplateProcessor<String> {

    private Jinjava jinjava;

    @Inject
    public JinjavaTemplateProcessor(final Configuration config, final ServletContext servletContext) {
        super(config, servletContext, "jinjava", "html", "js", "json");
        JinjavaConfig jinjavaConfig = new JinjavaConfig(Charsets.UTF_8, Locale.SIMPLIFIED_CHINESE, DateTimeZone.UTC, 10);

        jinjava = new Jinjava(jinjavaConfig);
        jinjava.setResourceLocator(new ServletContextResourceLocator());
    }

    @Override
    protected String resolve(String templatePath, Reader reader) throws Exception {
        return templatePath;
    }


    @Override
    public void writeTo(String templateReference,
                        Viewable viewable,
                        MediaType mediaType,
                        MultivaluedMap<String, Object> httpHeaders,
                        OutputStream out) throws IOException {
        final PrintStream ps = new PrintStream(out, true, getEncoding().toString());
        String template = Resources.toString(getServletContext().getResource(templateReference), getEncoding());
        ps.print(jinjava.render(template, (Map<String, ?>)viewable.getModel()));
    }

    private class ServletContextResourceLocator implements ResourceLocator {

        @Override
        public String getString(String fullName, Charset encoding, JinjavaInterpreter interpreter) throws IOException {
            ServletContext servletContext = JinjavaTemplateProcessor.this.getServletContext();
            return Resources.toString(servletContext.getResource(JinjavaTemplateProcessor.this.getBasePath() + "/" + fullName), JinjavaTemplateProcessor.this.getEncoding());
        }
    }
}

