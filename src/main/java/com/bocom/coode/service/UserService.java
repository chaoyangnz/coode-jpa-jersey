package com.bocom.coode.service;

import com.bocom.coode.dao.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Component
public class UserService {

    @PersistenceContext(unitName="coodePersistence")
    private EntityManager entityManager;

    public boolean authenticate(String userName, String password) {
        return true;
    }

    @Transactional
    public User queryOrCreateUser(String userName) {
        TypedQuery<User> queryUserByUserName = entityManager.createQuery("SELECT u FROM User u WHERE u.userName = ?1", User.class);
        User user = null;
        List<User> userList = queryUserByUserName.setParameter(1, userName).getResultList();
        if(userList.size() == 0) {
            user = new User();
            user.setUserName(userName);
            entityManager.persist(user);
        } else {
            user = userList.get(0);
        }

        return user;
    }
}
