package com.bocom.coode.service;

import com.bocom.coode.dao.entity.Node;
import com.bocom.coode.dao.entity.Tag;
import com.bocom.coode.dao.entity.nodes.PostContent;
import com.bocom.coode.helpers.Utils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@Component
public class NodeService {

    @PersistenceContext(unitName = "coodePersistence")
    private EntityManager entityManager;

    public PostContent queryPost(Long nodeId) {
        Node node = entityManager.find(Node.class, nodeId);
        if(node.isPost()) {
            return (PostContent)node;
        } else {
            throw new RuntimeException("not allowed");
        }
    }

    @Transactional
    public PostContent createPost(Long userId, int nodeType, String title, String content, String tagString) {
        PostContent post = PostContent.newInstance(nodeType);
        post.setTitle(title);
        post.setContent(content);
        post.setStatus(1);
        post.setIndexFlag(1);
        post.setCreatedAt(Utils.currentTime());
        post.setCreatedBy(userId);

        String[] tagNames = tagString.split(",");
        List<Tag> tags = new ArrayList<Tag>();
        for(String tagName : tagNames) {
            tags.add(queryTagOrNew(tagName));
        }
        post.setTags(tags);

        entityManager.persist(post);

        return post;
    }

    private Tag queryTagOrNew(String tagName) {
        TypedQuery<Tag> typedQuery = entityManager.createQuery("SELECT t FROM Tag t WHERE t.name = ?1", Tag.class);
        typedQuery.setParameter(1, tagName);
        List<Tag> tagList = typedQuery.getResultList();
        Tag tag = null;
        if(tagList.size() > 0) {
            tag = tagList.get(0);
        } else {
            tag = new Tag();
            tag.setName(tagName);
        }

        return tag;
    }
}
