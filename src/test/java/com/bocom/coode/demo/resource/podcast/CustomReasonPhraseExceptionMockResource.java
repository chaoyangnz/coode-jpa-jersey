package com.bocom.coode.demo.resource.podcast;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import com.bocom.coode.demo.errorhandling.CustomReasonPhraseException;
import com.bocom.coode.demo.service.PodcastService;
import org.springframework.beans.factory.annotation.Autowired;

@Path("/mocked-custom-reason-phrase-exception")
public class CustomReasonPhraseExceptionMockResource {
	
	@Autowired
	private PodcastService podcastService;
	
	@GET
	public void testReasonChangedInResponse() throws CustomReasonPhraseException {
		podcastService.generateCustomReasonPhraseException();
	}
}
